const opn = require('opn')

export default {
  head: {
    title: 'testApp',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: "stylesheet",
        href: "http://fonts.googleapis.com/css?family=Lato:400,700",
      }
    ]
  },
  hooks: {
    listen(server, { host, port }) {
      opn(`http://${host}:${port}`)
    }
  },

  css: [
    '@/assets/css/main.css',
  ],

  plugins: [
  ],
  

  components: true,

  buildModules: [
    '@nuxt/postcss8'
  ],

  modules: [
  ],

  build: {
    postcss: {
      plugins: {
        tailwindcss: {},
        autoprefixer: {},
      },
    },
  }
}
